package endpoint.filter;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.http.HttpServerRequest;

@Provider
public class RestFilter implements ContainerRequestFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(RestFilter.class);

	@Context
	UriInfo info;

	@Context
	HttpServerRequest request;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		final var method = requestContext.getMethod();
		final var path = info.getPath();
		final var ip = request.remoteAddress().toString();

		LOGGER.debug("Request {} {} {}", method, path, ip);
	}

}
