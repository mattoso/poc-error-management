package endpoint.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import api.exception.PFMException;

/**
 * Mapeamento de excecoes dos resources da aplicacao, cada vez que algum metodo
 * lanca {@link PFMException} esta classe e chamada imediatamente, e mapeia a
 * excecao para {@link Response}
 * 
 * @author Theodoro Mattoso
 * @see PFMException
 */
@Provider
public class ResourceExceptionMapper implements ExceptionMapper<PFMException> {

	@Override
	public Response toResponse(PFMException exception) {
		final var errorDTO = exception.getErrorDTO();
		return Response.status(errorDTO.getStatusCode()).entity(errorDTO).build();
	}

}
