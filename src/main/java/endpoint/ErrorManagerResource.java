package endpoint;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import service.ErrorManagerService;

@Path("/api/resources/v1/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "POC-Error Manager", description = "Operações de composição da POC")
public class ErrorManagerResource {

	@Inject
	ErrorManagerService service;
	
	@GET
	@Path("/hello")
	@APIResponse(responseCode = "200")
	@APIResponse(responseCode = "403", description = "Erro não tratado")
	@APIResponse(responseCode = "404", description = "Erro não tratado")
	@APIResponse(responseCode = "500", description = "Erro não tratado")
	@APIResponse(responseCode = "503", description = "Ação proibida")
	@Operation(summary = "Executa umn operação de consulta ao serviço.")
	public Response hello() {
		return Response.ok().entity(this.service.hello()).build();
	}
	
}
