package api.http;

import java.util.List;

import api.http.error.client.HTTPClientErrorCodes;
import api.http.error.server.HTTPServerErrorCodes;
import api.http.info.HTTPInfoCodes;
import api.http.redirect.HTTPRedirectCodes;
import api.http.success.HTTPSuccessCodes;

public final class HTTPStatusCodeValidator {

	private HTTPStatusCodeValidator() {
		throw new IllegalAccessError("Esta classe não pode produzir instancias dela mesma.");
	}
	
	public static boolean validateStatusCode(final int code) {
		final var checks = List.of(
			HTTPRedirectCodes.newInstance().isValid(code),
			HTTPClientErrorCodes.newInstance().isValid(code),
			HTTPServerErrorCodes.newInstance().isValid(code),
			HTTPInfoCodes.newInstance().isValid(code),
			HTTPSuccessCodes.newInstance().isValid(code)
		);
		return checks.contains(true);
	}
	
}
