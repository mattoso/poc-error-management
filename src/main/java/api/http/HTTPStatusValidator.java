package api.http;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Supplier;

public interface HTTPStatusValidator extends Supplier<List<Integer>> {

	default boolean isValid(final int code) {
		try {
			this.get().stream().filter(c -> c == code).findAny().orElseThrow();
			return true;
		} catch (NoSuchElementException nsee) {
			return false;
		}
 	}
	
}