package api.http.info;

import java.util.List;

import api.http.HTTPStatusValidator;

public class HTTPInfoCodes implements  HTTPStatusValidator {
	
	public static HTTPInfoCodes newInstance() {
		return new HTTPInfoCodes();
	}
	
	@Override
	public List<Integer> get() {
		return List.of(100, 101, 102, 103);
	}
 	
}
