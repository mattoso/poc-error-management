package api.http.success;

import java.util.List;

import api.http.HTTPStatusValidator;

public class HTTPSuccessCodes implements HTTPStatusValidator {

	public static HTTPSuccessCodes newInstance() {
		return new HTTPSuccessCodes();
	}
	
	@Override
	public List<Integer> get() {
		return List.of(200,201,202,203,204,205,206,207,208,226);
	}

}
