package api.http.redirect;

import java.util.List;

import api.http.HTTPStatusValidator;

public final class HTTPRedirectCodes implements HTTPStatusValidator {

	public static HTTPRedirectCodes newInstance() {
		return new HTTPRedirectCodes();
	}
	
	@Override
	public List<Integer> get() {
		return List.of(300,301,302,303,304,305,306,307,308);
	}

}
