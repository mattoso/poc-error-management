package api.http.error.server;

import java.util.List;

import api.http.HTTPStatusValidator;

public class HTTPServerErrorCodes implements HTTPStatusValidator {

	public static HTTPServerErrorCodes newInstance() {
		return new HTTPServerErrorCodes();
	}
	
	@Override
	public List<Integer> get() {
		return List.of(500,501,502,503,504,505,506,507,508,510,511);
	}
	
}
