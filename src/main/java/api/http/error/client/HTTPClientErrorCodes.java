package api.http.error.client;

import java.util.List;

import api.http.HTTPStatusValidator;

public class HTTPClientErrorCodes implements HTTPStatusValidator {

	public static HTTPClientErrorCodes newInstance() {
		return new HTTPClientErrorCodes();
	}
	
	@Override
	public List<Integer> get() {
		return List.of(400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417,
				418, 421, 422, 423, 424, 425, 426, 428, 429, 431, 451);
	}

}
