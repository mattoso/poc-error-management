package api.exception;

import java.util.function.Supplier;

import api.errors.http.Http403Mapper;
import api.errors.http.Http404Mapper;
import api.errors.http.Http500Mapper;
import api.errors.http.Http503Mapper;

/**
 * Dicionario de erros da aplicacao. Deve ser criado a partir de um documento
 * descrevendo cada tipo de erro de negocio refletido na API. Cada tipo de erro
 * deve ser um singleton, por estarmos em ambiente de nuvem de alta concorrencia
 * e multiplos acessos para cada um dos nossos endpoints.
 * 
 * @author Theodoro Mattoso
 */
public enum ErrorDictionary implements Supplier<PFMException> {

    ERRO_001(Http403Mapper.of("cause1", "action1", "message1")),
    ERRO_002(Http404Mapper.of("cause2", "action2", "message2")),
    ERRO_003(Http503Mapper.of("cause3", "action3", "message3")),
    ERRO_004(Http500Mapper.of("cause4", "action4", "message4"));

    private final PFMException exception;

    ErrorDictionary(final Supplier<PFMException> sup) {
        this.exception = sup.get();
    }

    @Override
    public PFMException get() {
        return this.exception;
    }
}