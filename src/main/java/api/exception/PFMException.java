package api.exception;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.isNull;

import javax.ws.rs.WebApplicationException;

import api.errors.PFMErrorDTO;

/**
 * Excecao principal da aplicacao que sera lancada para o mapeamento de erros do
 * frontend.
 * 
 * @author Theodoro Mattoso
 * @see PFMErrorDTO
 */
public class PFMException extends WebApplicationException {

    private final PFMErrorDTO errorDTO;

    private PFMException(PFMErrorDTO dto) {
        this.errorDTO = dto;
    }

    public static PFMException of(final PFMErrorDTO dto) {
        checkArgument(!isNull(dto), "O DTO de erro não pode ser nulo");
        return new PFMException(dto);
    }

    public PFMErrorDTO getErrorDTO() {
        return errorDTO;
    }

}
