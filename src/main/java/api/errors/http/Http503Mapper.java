package api.errors.http;

import api.errors.PFMErrorDTO;
import api.exception.PFMException;

/**
 * Mapeamento de erros 503.
 * 
 * @author Theodoro Mattoso
 * @see HttpErrorMapper
 * @see PFMErrorDTO
 * @see PFMException
 */
public class Http503Mapper implements HttpErrorMapper {

    private PFMErrorDTO dto;

    /**
     * Factory method para criacao do mapper, este metodo vai validar o estado da
     * aplicacao por proxy do factory methdo do {@link PFMErrorDTO}
     * 
     * @param cause   {@link String}
     * @param action  {@link String}
     * @param message {@link String}
     * @return {@link Http503Mapper}
     * @throws IllegalArgumentException caso o estado seja violado na construcao do
     *                                  {@link PFMErrorDTO}
     */
    public static Http503Mapper of(final String cause, final String action, final String message) {
        return new Http503Mapper(PFMErrorDTO.of(503, cause, action, message));
    }

    private Http503Mapper() {
        throw new IllegalAccessError("Esta classe nao pode produzir instancias dela mesma fora do metodo de fabrica.");
    }

    private Http503Mapper(final PFMErrorDTO dto) {
        this.dto = dto;
    }

    @Override
    public PFMErrorDTO getDto() {
        return this.dto;
    }

}
