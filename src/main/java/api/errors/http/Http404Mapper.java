package api.errors.http;

import api.errors.PFMErrorDTO;

/**
 * Mapeamento de erros 404.
 * 
 * @author Theodoro Mattoso
 * @see HttpErrorMapper
 * @see PFMErrorDTO
 * @see PFMException
 */
public class Http404Mapper implements HttpErrorMapper {

    private PFMErrorDTO dto;

    /**
     * Factory method para criacao do mapper, este metodo vai validar o estado da
     * aplicacao por proxy do factory methdo do {@link PFMErrorDTO}
     * 
     * @param cause   {@link String}
     * @param action  {@link String}
     * @param message {@link String}
     * @return {@link Http404Mapper}
     * @throws IllegalArgumentException caso o estado seja violado na construcao do
     *                                  {@link PFMErrorDTO}
     */
    public static Http404Mapper of(final String cause, final String action, final String message) {
        return new Http404Mapper(PFMErrorDTO.of(404, cause, action, message));
    }

    private Http404Mapper() {
        throw new IllegalAccessError("Esta classe nao pode produzir instancias dela mesma fora do metodo de fabrica.");
    }

    private Http404Mapper(final PFMErrorDTO dto) {
        this.dto = dto;
    }

    @Override
    public PFMErrorDTO getDto() {
        return this.dto;
    }

}
