package api.errors.http;

import api.errors.PFMErrorDTO;

/**
 * Mapeamento de erros 500.
 * 
 * @author Theodoro Mattoso
 * @see HttpErrorMapper
 * @see PFMErrorDTO
 * @see PFMException
 */
public class Http500Mapper implements HttpErrorMapper {

    private PFMErrorDTO dto;

    /**
     * Factory method para criacao do mapper, este metodo vai validar o estado da
     * aplicacao por proxy do factory methdo do {@link PFMErrorDTO}
     * 
     * @param cause   {@link String}
     * @param action  {@link String}
     * @param message {@link String}
     * @return {@link Http500Mapper}
     * @throws IllegalArgumentException caso o estado seja violado na construcao do
     *                                  {@link PFMErrorDTO}
     */
    public static Http500Mapper of(final String cause, final String action, final String message) {
        return new Http500Mapper(PFMErrorDTO.of(500, cause, action, message));
    }

    private Http500Mapper() {
        throw new IllegalAccessError("Esta classe nao pode produzir instancias dela mesma fora do metodo de fabrica.");
    }

    private Http500Mapper(final PFMErrorDTO dto) {
        this.dto = dto;
    }

    @Override
    public PFMErrorDTO getDto() {
        return this.dto;
    }

}
