package api.errors.http;

import api.errors.PFMErrorDTO;

/**
 * Mapeamento de erros 403.
 * 
 * @author Theodoro Mattoso
 * @see HttpErrorMapper
 * @see PFMErrorDTO
 * @see PFMException
 */
public final class Http403Mapper implements HttpErrorMapper {

    private PFMErrorDTO dto;

    /**
     * Factory method para criacao do mapper, este metodo vai validar o estado da
     * aplicacao por proxy do factory methdo do {@link PFMErrorDTO}
     * 
     * @param cause   {@link String}
     * @param action  {@link String}
     * @param message {@link String}
     * @return {@link Http403Mapper}
     * @throws IllegalArgumentException caso o estado seja violado na construcao do
     *                                  {@link PFMErrorDTO}
     */
    public static Http403Mapper of(final String cause, final String action, final String message) {
        return new Http403Mapper(PFMErrorDTO.of(403, cause, action, message));
    }

    private Http403Mapper() {
        throw new IllegalAccessError("Esta classe nao pode produzir instancias dela mesma fora do metodo de fabrica.");
    }

    private Http403Mapper(final PFMErrorDTO dto) {
        this.dto = dto;
    }

    @Override
    public PFMErrorDTO getDto() {
        return dto;
    }

}