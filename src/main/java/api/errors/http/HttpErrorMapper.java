package api.errors.http;

import java.util.function.Supplier;

import api.errors.PFMErrorDTO;
import api.exception.PFMException;

/**
 * Interface principal de provedor de excecoes do tipo {@link PFMException}
 * 
 * @author Theodoro Mattoso
 * @see PFMException
 */
public interface HttpErrorMapper extends Supplier<PFMException> {

    PFMErrorDTO getDto();

    @Override
    default PFMException get() {
        return PFMException.of(getDto());
    }

}
