package api.errors;

import static api.http.HTTPStatusCodeValidator.validateStatusCode;
import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;

/**
 * DTO de referencia que vai ser transportado pela aplicacao em caso de excecoes
 * no sistema.
 * 
 * @author Theodoro Mattoso
 */
public class PFMErrorDTO implements Comparable<PFMErrorDTO>, Serializable {

	private static final transient Comparator<PFMErrorDTO> COMPARATOR = Comparator
			.comparing((PFMErrorDTO pfme) -> pfme.cause).thenComparing(PFMErrorDTO::getAction)
			.thenComparing(PFMErrorDTO::getMessage);

	private int statusCode;

	private String cause;
	private String action;
	private String message;

	/**
	 * Factory method para construcao dessa classe. So podemos produzir instancias
	 * dessa classe se os parametros forem validos.
	 * 
	 * @param statusCode {@link Integer}
	 * @param cause      {@link String}
	 * @param action     {@link String}
	 * @param message    {@link String}
	 * @return {@link PFMErrorDTO}
	 * @throws IllegalArgumentException caso algum dos parametros passados nao
	 *                                  estejam em conformidade com o estado estavel
	 *                                  da aplicacao.
	 */
	public static PFMErrorDTO of(final int statusCode, final String cause, final String action, final String message) {
		checkArgument(validateStatusCode(statusCode), "So e possivel registrar codigos HTTP validos.");
		checkArgument(isNotBlank(cause), "A causa do erro deve ser informada, vinda diretamente do dicionario.");
		checkArgument(isNotBlank(action),
				"A acao para correcao do erro deve ser informada, vinda diretamente do dicionario.");
		checkArgument(isNotBlank(message), "A mensagem do erro deve ser informada, vinda diretamente do dicionario.");

		return new PFMErrorDTO(statusCode, cause, action, message);
	}

	/**
	 * Construtor padrao, so existe na classe para inicializar os campso em
	 * serializacao.
	 */
	public PFMErrorDTO() {
		this.statusCode = 0;
		this.cause = "";
		this.action = "";
		this.message = "";
	}

	private PFMErrorDTO(int statusCode, String cause, String action, String message) {
		this.statusCode = statusCode;
		this.cause = cause;
		this.action = action;
		this.message = message;
	}

	public int getStatusCode() {
		return this.statusCode;
	}

	public String getCause() {
		return this.cause;
	}

	public String getAction() {
		return this.action;
	}

	public String getMessage() {
		return this.message;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof PFMErrorDTO)) {
			return false;
		}
		PFMErrorDTO pFMError = (PFMErrorDTO) o;
		return statusCode == pFMError.statusCode && Objects.equals(cause, pFMError.cause)
				&& Objects.equals(action, pFMError.action) && Objects.equals(message, pFMError.message);
	}

	@Override
	public int hashCode() {
		return Objects.hash(statusCode, cause, action, message);
	}

	@Override
	public String toString() {
		return "{" + " statusCode='" + getStatusCode() + "'" + ", cause='" + getCause() + "'" + ", action='"
				+ getAction() + "'" + ", message='" + getMessage() + "'" + "}";
	}

	@Override
	public int compareTo(PFMErrorDTO o) {
		return COMPARATOR.compare(o, this);
	}

}
