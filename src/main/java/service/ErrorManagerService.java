package service;

import static api.exception.ErrorDictionary.*;

import javax.enterprise.context.RequestScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RequestScoped
public class ErrorManagerService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ErrorManagerService.class);

	public String hello() {
		try {
			throw new Exception("Mensagem de excecao que vai ser postada no log DEBUG.");
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw ERRO_001.get();
			// throw ERRO_002.get();
			// throw ERRO_003.get();
			// throw ERRO_004.get();
		}
	}
  
}